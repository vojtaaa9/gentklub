# gentklub

## Team

Design: pesek@webit.de
Frontend: pesek@webit.de
Project management: pesek@webit.de

## WCAG2

Level: A

## Usage

This is a kickstarted project:  https://github.com/webit-de/generator-kickstart

## Requirements

* [Bower](http://bower.io)
* [Bundler](http://bundler.io/)
* [Compass](http://compass-style.org/)
* [Grunt](http://gruntjs.com)
* [Node](https://nodejs.org/)
* [Ruby](https://www.ruby-lang.org/)
* [Sass CSS Importer Plugin](https://github.com/chriseppstein/sass-css-importer)
* [Sass](http://sass-lang.com/)

## How do I get setup?

1. npm install -g grunt-cli
1. gem install bundler
1. bundle install
1. npm install
1. grunt 