/**
 * TODO: add description
 * @module ScrollUp
 * @requires jquery
 * @requires jquery.exists
 * @author TODO: add author
 */
define(['jquery', 'jquery.exists'], function($, exists) {

  'use strict';

  var ScrollUp = {

    /**
     * Caches all jQuery Objects for later use.
     * @function _cacheElements
     * @private
     */
    _cacheElements: function() {
      this.$scroll_up = $('.scroll_up');
    },

    /**
     * Initiates the module.
     * @function init
     * @public
     */
    init: function() {
      ScrollUp._cacheElements();

      ScrollUp._bindEvents();
    },

    /**
     * Binds all events to jQuery DOM objects.
     * @function _bindEvents
     * @private
     */
    _bindEvents: function() {

      $.fn.scrollEnd = function(callback, timeout) {
        $(this).scroll(function(){
          var $this = $(this);
          if ($this.data('scrollTimeout')) {
            clearTimeout($this.data('scrollTimeout'));
          }
          $this.data('scrollTimeout', setTimeout(callback,timeout));
        });
      };
      
      // Takes care of Scrolling back to top
      $(window).scrollEnd(function(){

        if ($(this).scrollTop() > 100) {
          ScrollUp.$scroll_up.show();
        } else {
          ScrollUp.$scroll_up.hide();
        }
      }, 250);

      ScrollUp.$scroll_up.click(function () {
        $('html, body').animate({scrollTop: 0}, 500);
        return false;
      });

    }

  };

  return /** @alias module:ScrollUp */ {
    /** init */
    init: ScrollUp.init
  };

});
