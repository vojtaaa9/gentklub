/**
 * Main entry point into all Java Script.
 * @module Main
 * @requires jquery

 * @requires jquery.exists
 * @author pesek@webit.de
 */
require([
  'jquery',
  'jquery.exists',
  'login',
  'header',
  'add-post',
  'scroll-up'
], function(
  $,
  exists,
  Login,
  Header,
  AddPost,
  ScrollUp
) {

  'use strict';

  var Main = {
    /**
     * Caches all jQuery Objects for later use.
     * @function _cacheElements
     * @private
     */
    cacheElements: function() {
      // this.$bar = $('.bar');
    },
    /**
     * Initiates the module.
     * @function init
     * @public
     */
    init: function() {
      this.cacheElements();
      Login.init();
      Header.init();
      AddPost.init();
      ScrollUp.init();
    }
  };

  Main.init();

});
