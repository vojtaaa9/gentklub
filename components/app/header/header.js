/**
 * Handles interaction for Header
 * @module Header
 * @requires jquery
 * @requires jquery.exists
 * @author pesek@webit.de
 */
define(['jquery', 'jquery.exists'], function($, exists) {

  'use strict';

  var Header = {

    /**
     * Caches all jQuery Objects for later use.
     * @function _cacheElements
     * @private
     */
    _cacheElements: function() {
      this.$navigation = $('.navigation');
      this.$moreOpen = $('.nav-button');
      this.$moreMenu = $('.nav-menu');
    },

    /**
     * Initiates the module.
     * @function init
     * @public
     */
    init: function() {
      Header._cacheElements();

      Header.$navigation.exists(function() {
        Header._bindEvents();
      });
    },

    /**
     * Binds all events to jQuery DOM objects.
     * @function _bindEvents
     * @private
     */
    _bindEvents: function() {
      Header.$moreOpen.on('click', function() {
        Header.$moreMenu.fadeToggle();
      });
    }

  };

  return /** @alias module:Header */ {
    /** init */
    init: Header.init
  };

});
