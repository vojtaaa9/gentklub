/**
 * Login Validation
 * @module Login
 * @requires jquery
 * @requires jquery.exists
 * @author pesek@webit.de
 */
define(['jquery', 'jquery.exists'], function($, exists) {

  'use strict';

  var Login = {

    /**
     * Caches all jQuery Objects for later use.
     * @function _cacheElements
     * @private
     */
    _cacheElements: function() {
      this.$login = $('.login');
      this.$login_form = $('.login-form');
      this.$input_user = $('.login-input-username');
      this.$input_pass = $('.login-input-password');
      this.$login_error = $('.login-password-error');
      this.$login_forgot = $('.login-forgot');
      this.$reset_form = $('.reset-form');
      this.$input_reset = $('.input-resetpass');
      this.$reset_back = $('.reset-return');
      this.$reset_error = $('.reset-error');
      this.$reset_succes = $('.reset-succes');
    },

    /**
     * Initiates the module.
     * @function init
     * @public
     */
    init: function() {
      Login._cacheElements();
      Login._bindEvents();
    },

    /**
     * Binds all events to jQuery DOM objects.
     * @function _bindEvents
     * @private
     */
    _bindEvents: function() {
      // Binds login form
      Login.$login_form.on('submit', function(event) {
        event.preventDefault();
        Login._validate();
      });

      // Switch to reset form
      Login.$login_forgot.click(function() {
        Login.$login_form.hide();
        Login.$reset_form.fadeIn();
      });

      // Switch to login form
      Login.$reset_back.click(function() {
        Login.$reset_form.hide();
        Login.$login_form.fadeIn();
      });

      // Binds Reset pass form
      Login.$reset_form.on('submit', function(event) {
        event.preventDefault();
        var mail = Login.$input_reset.val();
        if(mail != 'vojtaaa9@gmail.com') {
          Login.$reset_error.slideDown(300);
          Login.$input_reset.focus();
        }
        else {
          if(Login.$reset_error.css('display') != 'none') {
            Login.$reset_error.slideUp(300);
          }
          Login.$reset_succes.slideDown(300);
        }
      });
    },

    _validate: function() {
      var user = Login.$input_user.val();
      var pass = Login.$input_pass.val();
      if(user == 'admin' && pass == '1234') {
          window.location.href = './main.html';
      } else {
        Login.$login_error.slideDown(300);
        Login.$input_pass.val('');
        Login.$input_user.focus();
      }
    }

  };

  return /** @alias module:Login */ {
    /** init */
    init: Login.init
  };

});
