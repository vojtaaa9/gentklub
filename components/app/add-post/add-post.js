/**
 * TODO: add description
 * @module AddPost
 * @requires jquery
 * @requires jquery.exists
 * @author TODO: add author
 */
define(['jquery', 'jquery.exists'], function($, exists) {

  'use strict';

  var AddPost = {

    /**
     * Caches all jQuery Objects for later use.
     * @function _cacheElements
     * @private
     */
    _cacheElements: function() {
      this.$add_post = $('.add-post');
    },

    /**
     * Initiates the module.
     * @function init
     * @public
     */
    init: function() {
      AddPost._cacheElements();

      AddPost._bindEvents();
    },

    /**
     * Binds all events to jQuery DOM objects.
     * @function _bindEvents
     * @private
     */
    _bindEvents: function() {
      // Show selected files
      $('.inputfile').each( function() {
    		var $input = $(this),
    			$label = $input.next('label'),
    			labelVal = $label.html();

    		$input.on('change', function(e) {
    			var fileName = '';

    			if(this.files && this.files.length > 1)
    				//TODO generate more labels
            console.log('more files: ' + fileName);
    			else if( e.target.value ) {
            // Get fileName
            fileName = e.target.value.split( '\\' ).pop();
            $label.addClass('post_file_upload');
            $label.text(fileName);
          }
    		});

    		// Firefox bug fix
    		$input
    		.on( 'focus', function(){ $input.addClass( 'has-focus' ); })
    		.on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
    	});
    }

  };

  return /** @alias module:AddPost */ {
    /** init */
    init: AddPost.init
  };

});
