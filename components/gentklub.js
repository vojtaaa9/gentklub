requirejs.config({
	'appdir': '../',
	'baseUrl': './',
	'paths': {
		//{{app}}
    'scroll-up': 'app/scroll-up/scroll-up',
    'add-post': 'app/add-post/add-post',
    'login': 'app/login/login',
    'header': 'app/header/header',
    'content': 'app/content/content',

		//{{libs}}

		'jquery.exists': 'libs/jquery.exists/jquery.exists',
		'jquery': 'libs/jquery/dist/jquery.min'
	},
	'shim': {
		'jquery.exists': ['jquery']
	}
});

requirejs(['app/main']);
